import { Component, NgZone } from '@angular/core';
import { BLE } from '@ionic-native/ble/ngx';
import { NavController, ToastController } from '@ionic/angular';
import { Remote } from '../_models/remote';

@Component({
  selector: 'app-settings',
  templateUrl: 'settings.page.html',
  styleUrls: ['settings.page.scss']
})
export class SettingsPage {
  devices: Remote[] = [];
  peripheral: Remote;
  statusMessage: string;

  constructor(
    public navCtrl: NavController,
    private toastCtrl: ToastController,
    private ble: BLE,
    private ngZone: NgZone
  ) {
    // var r = new Remote()

    // r.name = "test"
    // r.id = "XX-XX_XX_X"
    // r.rssi = 5
    // this.devices.push(r)
  }

  scan() {
    this.setStatus("Scanning for Bluetooth LE Devices");
    this.devices = []; // clear list

    this.ble.scan([], 5).subscribe(
      device => this.onDeviceDiscovered(device),
      error => this.scanError(error)
    );

    setTimeout(this.setStatus.bind(this), 5000, "Scan complete");
  }

  onDeviceDiscovered(device: Remote) {
    console.log("Discovered " + JSON.stringify(device, null, 2));
    this.ngZone.run(() => {
      //if(device.name.startsWith("SG_Remote_")) {
        this.devices.push(device);
      //}
      
    });
  }

  // If location permission is denied, you'll end up here
  async scanError(error) {
    this.setStatus("Error " + error);
    let toast = await this.toastCtrl.create({
      message: "Error scanning for Bluetooth low energy devices",
      position: "middle",
      duration: 5000
    });
    toast.present();
  }

  setStatus(message) {
    console.log(message);
    this.ngZone.run(() => {
      this.statusMessage = message;
    });
  }


  //connect

  bleConnect(device) {
    this.ble.connect(device.id).subscribe(
        peripheral => this.onConnected(peripheral),
        peripheral => this.onDeviceDisconnected(peripheral)
    );
  }

  BleDisconnect() {
      this.ble.disconnect(this.peripheral.id).then(
  () => console.log('Disconnected ' + JSON.stringify(this.peripheral)),
  () => console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral)));
  }


  onConnected(peripheral) {
  this.ngZone.run(() => {
    this.setStatus('');
    this.peripheral = peripheral;
    this.statusMessage = "connected with: "+ peripheral.name
    localStorage.setItem('device', peripheral.id);
  });
  }

  async onDeviceDisconnected(peripheral) {
  const toast = await this.toastCtrl.create({
    message: 'The peripheral unexpectedly disconnected',
    duration: 3000,
    position: 'middle'
  });
  this.statusMessage = "not connected";
  toast.present();
  }

  // Disconnect peripheral when leaving the page
  ionViewWillLeave() {
  console.log('ionViewWillLeave disconnecting Bluetooth');
    //this.BleDisconnect();
  }

}
