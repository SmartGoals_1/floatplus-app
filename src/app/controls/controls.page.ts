import { Component } from '@angular/core';
import { BLE } from '@ionic-native/ble/ngx';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-controls',
  templateUrl: 'controls.page.html',
  styleUrls: ['controls.page.scss']
})
export class ControlsPage {
  private rangeVal: number = 0;


  constructor(private ble: BLE, private toastCtrl: ToastController) {}

  stop() {
    this.rangeVal = 0;
    this.sendMessage()
  }

  setValue(event) {
    this.rangeVal = event.detail.value;
    //this.sendMessage()
  }

  sendMessage() {
    let hex: number = parseInt(this.rangeVal.toString(16));

    if(hex < 6 && this.rangeVal > 7) return;

    var data = new Uint8Array(5);
    data[0] = 0x05;
    data[1] = 0x10;
    data[2] = 0x11;
    data[3] = 0x00;
    data[4] = hex;

    console.log(hex)

    this.sendToast(data.toString())

    this.ble.writeWithoutResponse(localStorage.getItem('device'), '9e310001-a36c-e782-2d4f-fe2b117e96f0','9e310002-a36c-e782-2d4f-fe2b117e96f0', data.buffer)
  }

  private async sendToast(msg: string) {
    let toast = await this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'middle'
    });

    toast.present();
  }

}
