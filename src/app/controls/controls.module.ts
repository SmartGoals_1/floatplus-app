import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ControlsPage } from './controls.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { ControlsPageRoutingModule } from './controls-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    ControlsPageRoutingModule
  ],
  declarations: [ControlsPage]
})
export class ControlsPageModule {}
