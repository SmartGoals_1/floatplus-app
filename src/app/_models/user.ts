﻿export class User {
    id: number;
    email: string;
    username: string;
    first_name: string;
    last_name: string;
    preferred_position: string;
    age: string;
    height: string;
    weight: string;
    profile_picture_path: string;
}