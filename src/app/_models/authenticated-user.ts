import { User } from './user';

export class AuthenticatedUser {
    user: User;
    token: string;
}